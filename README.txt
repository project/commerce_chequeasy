README file for Commerce Chequeasy

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How it works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
This project integrates Chequeasy cheques payment system into the
Drupal Commerce payment and checkout systems.
https://www.chequeasy.com
* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_chequeasy
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/commerce_chequeasy


REQUIREMENTS
------------
This module requires the following modules:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* PHP cURL library (http://php.net/manual/en/curl.setup.php)
* PHP Mcrypt extenstion (http://php.net/manual/en/mcrypt.setup.php)
* Chequeasy Merchant account (https://www.chequeasy.com/souscrire.html)


INSTALLATION
------------
* Install as you would normally install a contributed drupal module.
  See: Installing modules (Drupal 7) documentation page for further information.
  https://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
* Permissions: There are no specific permissions for this module.
  The Payments permissions are to be used for configurations.
* Enable the default "Chequeasy" payment method or create a new one of this type.
* Configure "Chequeasy" payment
  Edit "Enable payment method: Chequeasy" action (settings).
  - Merchant reference : Merchant reference from Chequeasy merchant account;
  - Secret Key: Secret Key from Chequeasy merchant account;
  - Checkout only: If checked, only the Chequeasy requests for checkout orders will be processed;
  - Server version: auto-populated by Chequeasy for the given Merchant credentials.
  - Payment service: options auto-populated by Chequeasy for the given Merchant credentials.
  - Service paramaters: fieldset with some fields auto-populated by Chequeasy data
    for the selected service.
  - Extra options: fieldset with extra options for Chequeasy requests.


HOW IT WORKS
------------

* General considerations:
  - Shop owner must have an Chequeasy Merchant account
    https://www.chequeasy.com/souscrire.html
  - Customer should have a valid cheque number.

* Customer/Checkout workflow:
  This is an Off-Site payment method
  Redirect customer from checkout to the payment service and back.
  - Customer redirected to Chequeasy where the customer should enter a valid
    cheque number;
  - After the cheque confirmation on Chequeasy side the Customer will be
    redirected  back to the store to complete the order checkout and
    download the cheque document provided by Chequeasy.

* Back-end workflow:
  - After the cheque confirmation on Chequeasy side and customer redirection
    a pending payment transaction will be created.
  - There are also updates for those transactions if there are
    updates done in the Chequeasy Backoffice Merchant account.
    The transactions could be canceled or validated (payment received).


TROUBLESHOOTING
---------------
* No troubleshooting pending for now.


MAINTAINERS
-----------
Maintainers:
* Tavi Toporjinschi (vasike) - https://www.drupal.org/u/vasike

This project has been developed by:
* Commerce Guys by Actualys,
  Visit https://www.commerceguys.fr for more information.
